mdf2iso
==================

mdf2iso is a simple utility to convert an Alcohol 120% bin image into
standard ISO-9660 format, cue sheets, or TOC file.

Online presence
---------------

* The mdf2iso homepage is hosted at
  <http://BSDforge.com/projects/sysutils/mdf2iso/>

* Source, Patches, bug reports, discussions and support are on Gitlab:
  <https://gitlab.com/bsdforge/mdf2iso>
